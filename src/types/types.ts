import { ApplicationActionType } from "../reducers/application/types";
import { UserActionType } from "../reducers/user/types";
import { DataActionType } from "../reducers/data/types";
export interface IComment {
  id: number;
  authorName: string;
  text: string;
}

export interface ICard {
  id: number;
  cardName: string;
  description: string;
  comments: Array<IComment>;
  author: string;
}

export interface IList {
  id: number;
  listName: string;
  cards: Array<ICard>;
}

export interface IUserData {
  firstName: string;
}

export enum TypeModal {
  REGISTRATION = `modal__window--reg`,
  CARD = `modal__window--card`,
}

export type DispatchType = ApplicationActionType | UserActionType | DataActionType;
