import React from "react";
import "./Comments.css";
import FromAddNewData from "../Form-add-new-data";
import { ActionCreator as DataActionCreator } from "../../reducers/data/actions";
import { getNewComment } from "../../utils";
import Comment from "../Comment/Comment";
import { withEdit } from "../../hocs/with-edit/with-edit";
import { ICard } from "../../types/types";
import { useDispatch } from "react-redux";

const WithComment = withEdit(Comment);

interface CommentsProps {
  card: ICard;
  isEdit: boolean;
  onChangeStatusEdit: () => void;
}

const Comments: React.FC<CommentsProps> = ({
  card,
  isEdit,
  onChangeStatusEdit,
}) => {
  const { comments } = card;
  const dispatch = useDispatch();

  const handleAddNewComment = (textValue: string) => {
    const newComment = getNewComment(textValue, comments);
    dispatch(DataActionCreator.addNewCommentCard(card.id, newComment));
  };

  return (
    <div className="detailed-card__comments comments">
      <h3 className="comments__title">Comments</h3>
      <ul className="comments__lists comments-lists">
        {comments.map((comment, index) => {
          return (
            <WithComment
              cardId={card.id}
              comment={comment}
              key={`${comment.text} ${index}`}
            />
          );
        })}
      </ul>
      <div className="comments__add">
        {isEdit ? (
          <FromAddNewData
            title={`Comments`}
            onSubmitForm={handleAddNewComment}
            onCancelForm={onChangeStatusEdit}
          />
        ) : (
          <p
            className="comments__add-text"
            tabIndex={0}
            onClick={onChangeStatusEdit}
          >
            Add a new comment...
          </p>
        )}
      </div>
    </div>
  );
};

export default Comments;
