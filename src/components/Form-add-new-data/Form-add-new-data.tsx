import React, { Component } from "react";
import "./Form-add-new-data.css";

interface FormAddNewDataProps {
  onSubmitForm: (
    textValue: string,
  ) => void;
  onCancelForm: () => void;
  title: string;
}

class FormAddNewData extends Component<FormAddNewDataProps> {
  private inputRef = React.createRef<HTMLInputElement>();

  constructor(props: FormAddNewDataProps) {
    super(props);
    this.inputRef = React.createRef();
    this.handleSubmitForm = this.handleSubmitForm.bind(this);
  }

  handleSubmitForm(evt: React.FormEvent<HTMLElement>): void {
    evt.preventDefault();

    if (this.inputRef.current !== null) {
      const inputValue = this.inputRef.current.value;
      this.props.onSubmitForm(
        inputValue,
      );
      this.props.onCancelForm();
    }
  }

  render() {
    const { onCancelForm, title } = this.props;

    return (
      <form
        className="lists__form-add form-add"
        action="#"
        method="post"
        onSubmit={this.handleSubmitForm}
      >
        <div className="form-group">
          <label htmlFor="listName">{title}</label>
          <input
            type="text"
            className="form-control"
            id="listName"
            placeholder={`Enter the name of the ${title.toLowerCase()}`}
            ref={this.inputRef}
          />
        </div>
        <button type="submit" className="btn btn-success">
          Add
        </button>
        <button
          type="button"
          className="close"
          aria-label="Close adding"
          onClick={onCancelForm}
        >
          <span aria-hidden="true">&times;</span>
        </button>
      </form>
    );
  }
}

export default FormAddNewData;
