import React from "react";
import "./Description-card.css";
import FormEdit from "../Form-edit";
import { ICard } from "../../types/types";

interface DescriptionCardProps {
  card: ICard;
  isEdit: boolean;
  onChangeStatusEdit: () => void;
  onChangeDescription: (evt: React.ChangeEvent<HTMLTextAreaElement>) => void;
  onSaveCard: () => void;
}

const DescriptionCard: React.FC<DescriptionCardProps> = ({
  card,
  isEdit,
  onChangeStatusEdit,
  onChangeDescription,
  onSaveCard,
}) => {
  const { description } = card;
  const isEmptyDescription = !description.length;

  return (
    <div className="detailed-card__description-block description-card">
      <h3 className="description-card__title">Description</h3>
      {isEdit ? (
        <FormEdit
          textEdit={card.description}
          onCancelForm={onChangeStatusEdit}
          onSubmitForm={onSaveCard}
          onChange={onChangeDescription}
        />
      ) : (
        <div className="detailed-card__description">
          {isEmptyDescription ? (
            <p
              className="detailed-card__description--add"
              onClick={onChangeStatusEdit}
              tabIndex={0}
            >
              Add detailed description...
            </p>
          ) : (
            <p
              className="detailed-card__description--text"
              onClick={onChangeStatusEdit}
              tabIndex={0}
            >
              {description}
            </p>
          )}
        </div>
      )}
    </div>
  );
};

export default DescriptionCard;
