import React from "react";
import iconComment from "../../images/icon-comment.svg";
import { ICard } from "../../types/types";

interface CardInfoShortProps {
  card: ICard;
}

const CardInfoShort: React.FC<CardInfoShortProps> = ({
  card: { comments },
}) => {
  const isNoEmptyComments = !!comments.length;
  let commentElements;

  if (isNoEmptyComments) {
    commentElements = (
      <li className="card-info-short__item d-flex align-items-center">
        <img
          src={iconComment}
          className="mr-1"
          alt="icon Comment"
          width="12"
          height="12"
        />
        {comments.length}
      </li>
    );
  }

  return (
    <ul className={`card__info-short card-info-short d-block w-100 p-0 m-0`}>
      {commentElements}
    </ul>
  );
};

export default CardInfoShort;
