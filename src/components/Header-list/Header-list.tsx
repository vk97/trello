import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { ActionCreator as DataActionCreator } from "../../reducers/data/actions";
import "./Header-list.css";
import { IList } from "../../types/types";

interface HeaderListProps {
  list: IList;
  isEdit: boolean;
  onChangeStatusEdit: () => void;
}

const HeaderList: React.FC<HeaderListProps> = ({ list, isEdit, onChangeStatusEdit }) => {
  const [listState, updateList] = useState(list);
  const dispatch = useDispatch();

  const handleChangeTitle = (evt: React.ChangeEvent<HTMLTextAreaElement>) => {
    const target = evt.target as HTMLTextAreaElement;
    const value = target.value;

    updateList((prevState) => ({
      ...prevState,
      listName: value,
    }));
  };

  const handleSaveList = () => {
    const isNoEmpty = listState.listName.length;
    if (isNoEmpty) {
      dispatch(DataActionCreator.updateLists(listState));
      onChangeStatusEdit();
    } else {
      updateList(list);
      onChangeStatusEdit();
    }
  };

  return (
    <div className="list-header">
      {isEdit ? (
        <textarea
          autoFocus
          className="list-header__title-edit form-control"
          onBlur={handleSaveList}
          onChange={handleChangeTitle}
          value={listState.listName}
        ></textarea>
      ) : (
        <h3 className="list-header__title" onClick={onChangeStatusEdit}>
          {list.listName}
        </h3>
      )}
    </div>
  );
};

export default HeaderList;
