import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { ActionCreator as DataActionCreator } from "../../reducers/data/actions";
import { getBoardLists } from "../../reducers/data/selectors";
import { getNewList } from "../../utils";
import FormAddNewData from "../Form-add-new-data";

interface NewListProps {
  isEdit: boolean;
  onChangeStatusEdit: () => void;
}

const NewList: React.FC<NewListProps> = ({ isEdit, onChangeStatusEdit }) => {
  const lists = useSelector(getBoardLists);
  const dispatch = useDispatch();

  const handleAddNewList = (textValue: string): void => {
    dispatch(DataActionCreator.addNewList(getNewList(textValue, lists)));
  };

  return (
    <div className="lists__card-add">
      {isEdit ? (
        <FormAddNewData
          title="List"
          onSubmitForm={handleAddNewList}
          onCancelForm={onChangeStatusEdit}
        />
      ) : (
        <button
          className="btn btn-secondary"
          type="button"
          onClick={onChangeStatusEdit}
        >
          Add new list
        </button>
      )}
    </div>
  );
};

export default NewList;
