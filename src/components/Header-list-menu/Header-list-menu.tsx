import React from "react";
import "./Header-list-menu.css";
import imageDots from "../../images/icon-dots.svg";
import ListActionsLists from "../List-actions-list";
import { IList } from "../../types/types";

interface HeaderListMenuProps {
  isEdit: boolean;
  list: IList;
  onChangeStatusEdit: () => void;
}

const HeaderListMenu: React.FC<HeaderListMenuProps> = ({
  isEdit,
  list,
  onChangeStatusEdit,
}) => {
  let listActionElement;

  if (isEdit) {
    listActionElement = (
      <div className="header-list-menu__list-actions list-actions">
        <div className="list-actions__header">
          <span className="header-list-menu__title">List actions</span>
          <button
            type="button"
            className="header-list-menu__button-close close"
            aria-label="Close adding"
            onClick={onChangeStatusEdit}
          >
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <ListActionsLists list={list} />
      </div>
    );
  }

  return (
    <div className="list-header__menu header-list-menu">
      <button
        className="header-list-menu__button"
        type="button"
        onClick={onChangeStatusEdit}
      >
        <img src={imageDots} width="20" alt="" />
      </button>
      {listActionElement}
    </div>
  );
};

export default HeaderListMenu;
