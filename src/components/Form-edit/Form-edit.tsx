import React, { Component } from "react";

interface FormEditProps {
  onSubmitForm: () => void;
  onCancelForm: () => void;
  onChange: (evt: React.ChangeEvent<HTMLTextAreaElement>) => void;
  textEdit: string;
}

class FormEdit extends Component<FormEditProps> {
  private inputRef = React.createRef<HTMLTextAreaElement>();

  constructor(props: FormEditProps) {
    super(props);
    this.inputRef = React.createRef();
    this.handleSubmitForm = this.handleSubmitForm.bind(this);
  }

  handleSubmitForm(evt: React.FormEvent<HTMLElement>): void {
    evt.preventDefault();
    this.props.onSubmitForm();
    this.props.onCancelForm();
  }

  render() {
    const { onCancelForm, onChange, textEdit } = this.props;

    return (
      <form
        className="lists__form-add form-add"
        action="#"
        method="post"
        onSubmit={this.handleSubmitForm}
      >
        <div className="form-group">
          <label htmlFor="listName">Description card</label>
          <textarea
            className="form-control"
            id="listName"
            placeholder="Enter description card"
            ref={this.inputRef}
            value={textEdit}
            onChange={onChange}
          ></textarea>
        </div>
        <button type="submit" className="btn btn-success">
          Save
        </button>
        <button
          type="button"
          className="close"
          aria-label="Close adding"
          onClick={onCancelForm}
        >
          <span aria-hidden="true">&times;</span>
        </button>
      </form>
    );
  }
}

export default FormEdit;
