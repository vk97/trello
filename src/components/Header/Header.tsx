import React from "react";
import "./Header.css";
import iconUser from "../../images/icon-user.svg";
import { IUserData } from "../../types/types";

interface HeaderProps {
  userData: IUserData;
  onOpenModal: () => void;
}

const Header: React.FC<HeaderProps> = ({ userData, onOpenModal }) => {
  const userName = userData.firstName || `Sign In`;

  return (
    <header className="header">
      <div className="header__content container">
        <div className="col-xl-3">
          <a href="#!" className="header__logo">
            <span className="header__logo">Trello</span>
          </a>
        </div>
        <div className="col-xl-3 ml-auto">
          <div
            className="header__profile d-flex justify-content-end align-items-center profile"
            onClick={onOpenModal}
          >
            <img
              className="profile__photo"
              src={iconUser}
              alt="User avatar"
            />
            <div className="profile__name">{userName}</div>
          </div>
        </div>
      </div>
    </header>
  );
};

export default Header;
