import React from "react";
import "./Header-card.css";
import { ICard } from "../../types/types";

interface HeaderCardProps {
  card: ICard;
  isEdit: boolean;
  onChangeStatusEdit: () => void;
  onChangeCard: (evt: React.ChangeEvent<HTMLTextAreaElement>) => void;
  onSaveCard: () => void;
}

const HeaderCard: React.FC<HeaderCardProps> = ({
  card,
  isEdit,
  onChangeStatusEdit,
  onChangeCard,
  onSaveCard,
}) => {
  const handleSaveCard = () => {
    onSaveCard();
    onChangeStatusEdit();
  };

  return (
    <React.Fragment>
      {isEdit ? (
        <textarea
          autoFocus
          className="card__title-edit form-control"
          onBlur={handleSaveCard}
          onChange={onChangeCard}
          value={card.cardName}
        ></textarea>
      ) : (
        <h4 className="card__title m-0" onClick={onChangeStatusEdit}>
          {card.cardName}
        </h4>
      )}
    </React.Fragment>
  );
};

export default HeaderCard;
