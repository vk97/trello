import React from "react";
import { useSelector, useDispatch } from "react-redux";
import "./New-card.css";
import { ActionCreator as DataActionCreator } from "../../reducers/data/actions";
import { getBoardLists } from "../../reducers/data/selectors";
import FormAddNewData from "../Form-add-new-data";
import { getNewCard } from "../../utils";


interface NewCardProps {
  listId: number;
  isEdit: boolean;
  onChangeStatusEdit: () => void;
}

const NewCard: React.FC<NewCardProps> = ({ listId, isEdit, onChangeStatusEdit }) => {
  const lists = useSelector(getBoardLists);
  const dispatch = useDispatch();

  const handleAddNewCard = (textValue: string) => {
    dispatch(
      DataActionCreator.addNewCard(getNewCard(textValue, lists), listId)
    );
  };

  return (
    <div className="lists__card-add new-card">
      {isEdit ? (
        <FormAddNewData
          title="Card"
          onSubmitForm={handleAddNewCard}
          onCancelForm={onChangeStatusEdit}
        />
      ) : (
        <button
          className="new-card__button btn btn-primary"
          type="button"
          onClick={onChangeStatusEdit}
        >
          + Add new card
        </button>
      )}
    </div>
  );
};

export default NewCard;
