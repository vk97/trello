import React, { Component } from "react";
import { IUserData } from "../../types/types";

interface FormRegistrationProps {
  onSubmitForm: (userData: IUserData) => void;
}

class FormRegistration extends Component<FormRegistrationProps> {
  private inputFirstName = React.createRef<HTMLInputElement>();

  constructor(props: FormRegistrationProps) {
    super(props);
    this.inputFirstName = React.createRef();
    this.handleRegistrationUser = this.handleRegistrationUser.bind(this);
  }

  handleRegistrationUser(evt: React.FormEvent<HTMLElement>): void {
    evt.preventDefault();

    if (this.inputFirstName.current !== null) {
      const userData = {
        firstName: this.inputFirstName.current.value,
      };

      this.props.onSubmitForm(userData);
    }
  }

  render() {
    return (
      <form className="form-registration">
        <div className="form-group">
          <label htmlFor="first-name">First Name</label>
          <input
            type="text"
            className="form-control"
            id="first-name"
            placeholder="Enter first name"
            ref={this.inputFirstName}
          />
        </div>
        <button
          type="submit"
          className="btn btn-primary"
          onClick={this.handleRegistrationUser}
        >
          Register
        </button>
      </form>
    );
  }
}

export default FormRegistration;
