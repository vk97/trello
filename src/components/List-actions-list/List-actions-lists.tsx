import React from "react";
import { connect } from "react-redux";
import { ActionCreator as DataActionCreator } from "../../reducers/data/actions";
import { IList, DispatchType } from "../../types/types";

interface ListActionsListsProps {
  list: IList;
  onDeleteList: (list: IList) => void;
}

const ListActionsLists: React.FC<ListActionsListsProps> = ({ list, onDeleteList }) => {
  const handleDeleteList = (evt: React.FormEvent<HTMLElement>) => {
    evt.preventDefault();
    onDeleteList(list);
  };

  return (
    <ul className="list-actions__lists list-actions-lists">
      <li className="list-actions-lists__item">
        <a
          href="#!"
          className="list-actions-lists__link"
          onClick={handleDeleteList}
        >
          Delete list
        </a>
      </li>
    </ul>
  );
};

const mapDispatchToProps = (dispatch: (action: DispatchType) => void) => ({
  onDeleteList: (list: IList) => {
    dispatch(DataActionCreator.deleteList(list));
  },
});

export { ListActionsLists };
export default connect(null, mapDispatchToProps)(ListActionsLists);
