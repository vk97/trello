import React, { Component } from "react";
import "./Modal.css";
import Portal from "../Portal";
import { closeModals } from "../../utils";
import { TypeModal } from "../../types/types";

interface ModalProps {
  isOpen: boolean;
  title?: string;
  onCancel: () => void;
  children: React.ReactNode;
  classModal: TypeModal;
}

class Modal extends Component<ModalProps> {
  constructor(props: ModalProps) {
    super(props);
  }

  handleCloseModal = (evt: KeyboardEvent): void => {
    closeModals.isEscPress(evt, this.props.onCancel);
  }

  componentDidMount() {
    window.addEventListener("keydown", this.handleCloseModal);
  }

  componentWillUnmount() {
    window.removeEventListener("keydown", this.handleCloseModal);
  }

  render() {
    const { isOpen, title, onCancel, children, classModal } = this.props;
    const isEmptyTitle = title || ``;

    return (
      <React.Fragment>
        {isOpen && (
          <Portal>
            <div className="modal row">
              <div className="modal__overlay">
                <div
                  className={`modal__window ${classModal} col-10 col-sm-6 col-xl-3 `}
                >
                  <div className="modal__content ">
                    {isEmptyTitle ? (
                      <h2 className="modal__title">{title}</h2>
                    ) : (
                      ``
                    )}
                    <div className="modal__button modal__body">{children}</div>
                    <button
                      type="button"
                      className="modal__button-close close"
                      aria-label="Close"
                      onClick={onCancel}
                    >
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </Portal>
        )}
      </React.Fragment>
    );
  }
}

export default Modal;
