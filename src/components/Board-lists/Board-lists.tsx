import React from "react";
import BoardList from "../Board-list";
import "./Board-lists.css";
import NewList from "../New-list";
import { IList } from "../../types/types";
import { withEdit } from "../../hocs/with-edit/with-edit";

const WithNewList = withEdit(NewList);
interface BoardListsProps {
  lists: Array<IList>;
}

const BoardLists: React.FC<BoardListsProps> = ({ lists }) => {
  return (
    <div className="page-main__list lists row flex-nowrap">
      {lists.map((list, index) => {
        return <BoardList list={list} key={`${list.listName} ${index}`} />;
      })}
      <WithNewList />
    </div>
  );
};

export default BoardLists;
