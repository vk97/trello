import React from "react";
import "./Board-list.css";
import HeaderList from "../Header-list";
import Card from "../Card";
import { withEdit } from "../../hocs/with-edit/with-edit";
import HeaderListMenu from "../Header-list-menu";
import NewCard from "../New-card";
import { IList } from "../../types/types";

const WithEditListMenu = withEdit(HeaderListMenu);
const WithNewCard = withEdit(NewCard);
const WithHeaderList = withEdit(HeaderList);

interface BoardListProps {
  list: IList;
}

const BoardList: React.FC<BoardListProps> = ({ list }) => {
  return (
    <div className="lists__list list mr-3">
      <div className="list__content">
        <div className="list__header d-flex justify-content-between align-items-center mb-3">
          <WithHeaderList list={list} />
          <WithEditListMenu list={list} />
        </div>
        <div className="list__card-list card-list">
          {list.cards.map((card, index) => {
            return (
              <Card list={list} card={card} key={`${card.cardName}${index}`} />
            );
          })}
          <WithNewCard listId={list.id} />
        </div>
      </div>
    </div>
  );
};

export default BoardList;
