import React from "react";
import "./Detailed-card.css";
import HeaderCard from "../Header-card";
import { withEdit } from "../../hocs/with-edit/with-edit";
import DescriptionCard from "../Description-card";
import Comments from "../Comments";
import { IList, ICard } from "../../types/types";

const WithEditHeaderCard = withEdit(HeaderCard);
const WithEditDescriptionCard = withEdit(DescriptionCard);
const WithEditComments = withEdit(Comments);

interface DetailedCardProps {
  list: IList;
  card: ICard;
  cardState: ICard;
  onChangeTitle: (evt: React.ChangeEvent<HTMLTextAreaElement>) => void;
  onChangeDescription: (evt: React.ChangeEvent<HTMLTextAreaElement>) => void;
  onSaveCard: () => void;
  onDeleteCard: () => void;
}

const DetailedCard: React.FC<DetailedCardProps> = ({
  list,
  card,
  cardState,
  onDeleteCard,
  onChangeTitle,
  onChangeDescription,
  onSaveCard,
}) => {
  const { author } = card;

  return (
    <div className="detailed-card">
      <WithEditHeaderCard
        card={cardState}
        onChangeCard={onChangeTitle}
        onSaveCard={onSaveCard}
      />
      <div className="detailed-card__list-info row">
        <p className="col-sm-12 col-xl-6">
          In the column of
          <span className="detailed-card__list-name"> {list.listName}</span>
        </p>

        <p className="col-sm-12 col-xl-6">
          Card author:
          <span className="detailed-card__author-name">{author}</span>
        </p>
      </div>
      <div className="detailed-card__content">
        <WithEditDescriptionCard
          card={cardState}
          onChangeDescription={onChangeDescription}
          onSaveCard={onSaveCard}
        />
        <WithEditComments card={card} />
      </div>
      <button type="button" className="btn btn-danger" onClick={onDeleteCard}>
        Delete card
      </button>
    </div>
  );
};

export default DetailedCard;
