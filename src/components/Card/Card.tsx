import React, { useState } from "react";
import "./Card.css";
import iconEdit from "../../images/icon-edit.svg";
import CardInfoShort from "../Card-info-short";
import Modal from "../Modal";
import DetailedCard from "../Detailed-card";
import { ActionCreator as ApplicationActionCreator } from "../../reducers/application/actions";
import { ActionCreator as DataActionCreator } from "../../reducers/data/actions";
import { getActiveCardModal } from "../../reducers/application/selectors";
import { TypeModal } from "../../types/types";
import { IList, ICard } from "../../types/types";
import { useSelector, useDispatch } from "react-redux";

interface CardProps {
  list: IList;
  card: ICard;
}

const Card: React.FC<CardProps> = ({ list, card }) => {
  const [cardState, updateCard] = useState(card);
  const activeCardModal = useSelector(getActiveCardModal);
  const dispatch = useDispatch();
  const isOpenModal = activeCardModal === card.id;
  let modal;

  const checkEmptyField = (field: string): boolean => {
    return field.replace(/\s+/g, "") ? true : false;
  };

  const handleChangeTitle = (
    evt: React.ChangeEvent<HTMLTextAreaElement>
  ): void => {
    const target = evt.target as HTMLTextAreaElement;
    const value = target.value;

    updateCard((prevState) => ({
      ...prevState,
      cardName: value,
    }));
  };

  const handleChangeDescription = (
    evt: React.ChangeEvent<HTMLTextAreaElement>
  ): void => {
    const target = evt.target as HTMLTextAreaElement;
    const value = target.value;

    updateCard((prevState) => ({
      ...prevState,
      description: value,
    }));
  };

  const handleSaveCard = (): void => {
    const isNoEmpty = checkEmptyField(cardState.cardName);
    if (isNoEmpty) {
      dispatch(DataActionCreator.updateCard(cardState, list.id));
    } else {
      updateCard(card);
    }
  };

  const handleDeleteCard = (): void => {
    dispatch(DataActionCreator.deleteCard(card.id, list.id));
  };

  const handleChangeActiveCard = (cardId: number): void => {
    dispatch(ApplicationActionCreator.changeActiveCardModal(cardId));
  };

  if (isOpenModal) {
    modal = (
      <Modal
        isOpen={isOpenModal}
        onCancel={handleChangeActiveCard.bind(null, -1)}
        classModal={TypeModal.CARD}
      >
        <DetailedCard
          list={list}
          card={card}
          cardState={cardState}
          onChangeTitle={handleChangeTitle}
          onChangeDescription={handleChangeDescription}
          onSaveCard={handleSaveCard}
          onDeleteCard={handleDeleteCard}
        />
      </Modal>
    );
  }

  return (
    <React.Fragment>
      <div className="card-list__card card d-flex flex-wrap justify-content-between align-items-center flex-row mb-2 p-2">
        <a
          href="#!"
          className="card__link-edit"
          onClick={handleChangeActiveCard.bind(null, card.id)}
        >
          <h4 className="card__title m-0">{card.cardName}</h4>
        </a>
        <a
          href="#!"
          className="card__link-edit card__link-edit--button"
          onClick={handleChangeActiveCard.bind(null, card.id)}
        >
          <img src={iconEdit} alt="Button edit card" width="12" />
        </a>
        <CardInfoShort card={card} />
      </div>
      {modal}
    </React.Fragment>
  );
};

export default Card;
