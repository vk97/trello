import React, { useState } from "react";
import FormEdit from "../Form-edit";
import { IComment } from "../../types/types";
import { useDispatch } from "react-redux";
import { ActionCreator as DataActionCreator } from "../../reducers/data/actions";

interface CommentProps {
  isEdit: boolean;
  cardId: number;
  comment: IComment;
  onChangeStatusEdit: () => void;
}

const Comment: React.FC<CommentProps> = ({
  isEdit,
  cardId,
  comment,
  onChangeStatusEdit,
}) => {
  const [commentState, updateComment] = useState(comment);
  const dispatch = useDispatch();

  const handleDelete = () => {
    dispatch(DataActionCreator.deleteCommentCard(cardId, comment.id));
  };

  const handleChange = (evt: React.ChangeEvent<HTMLTextAreaElement>) => {
    const target = evt.target as HTMLTextAreaElement;
    const value = target.value;

    updateComment((prevState) => ({
      ...prevState,
      text: value,
    }));
  };

  const handleSave = () => {
    dispatch(DataActionCreator.updateComments(cardId, commentState));
  };

  if (!isEdit) {
    return (
      <li className="comment-lists__item">
        <div className="comment-lists__author">
          <span className="comment-lists__author-name">
            {comment.authorName}
          </span>
        </div>
        <p className="comment-lists__comment">
          <span className="comment-lists__text">{comment.text}</span>
        </p>
        <a
          href="#!"
          className="comment-lists__link"
          onClick={onChangeStatusEdit}
        >
          Change
        </a>
        <a
          href="#!"
          className="comment-lists__link"
          data-comment-id={comment.id}
          onClick={handleDelete}
        >
          Delete
        </a>
      </li>
    );
  }

  return (
    <FormEdit
      textEdit={commentState.text}
      onSubmitForm={handleSave}
      onChange={handleChange}
      onCancelForm={onChangeStatusEdit}
    />
  );
};

export default Comment;
