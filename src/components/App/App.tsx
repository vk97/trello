import React from "react";
import { connect } from "react-redux";
import "./App.css";
import Header from "../Header";
import NoAuth from "../No-auth";
import BoardLists from "../Board-lists";
import Modal from "../Modal";
import FormRegistration from "../Form-registration";
import { getBoardLists } from "../../reducers/data/selectors";
import { getUserData } from "../../reducers/user/selectors";
import { getStatusModalReg } from "../../reducers/application/selectors";
import { ActionCreator as UserActionCreator } from "../../reducers/user/actions";
import { AuthStatus } from "../../const";
import { ActionCreator as ApplicationActionCreator } from "../../reducers/application/actions";
import { IList, IUserData, TypeModal } from "../../types/types";
import { RootState } from "../../reducers/reducer";
import { DispatchType } from "../../types/types";

interface IData {
  lists: Array<IList>;
  userData: IUserData;
  isModalRegOpen: boolean;
}

interface IFunction {
  onChangeStatusModalReg: (statusModal: boolean) => void;
  onRegistration: (userData: IUserData) => void;
}

type AppProps = IData & IFunction;

const App: React.FC<AppProps> = ({
  lists,
  userData,
  isModalRegOpen,
  onRegistration,
  onChangeStatusModalReg,
}) => {
  let modal;
  const isEmptyUserData = userData.firstName.length === 0;

  if (isEmptyUserData) {
    modal = (
      <Modal
        isOpen={isModalRegOpen}
        onCancel={onChangeStatusModalReg.bind(null, false)}
        title="Register New Account"
        classModal={TypeModal.REGISTRATION}
      >
        <FormRegistration onSubmitForm={onRegistration} />
      </Modal>
    );
  }

  return (
    <React.Fragment>
      <Header
        userData={userData}
        onOpenModal={onChangeStatusModalReg.bind(null, true)}
      />

      <main className="page-main d-flex">
        <h2 className="visually-hidden">
          List of cards from the "Name board" board
        </h2>
        <div className="page-main__container container">
          {isEmptyUserData ? (
            <NoAuth onOpenModal={onChangeStatusModalReg.bind(null, true)} />
          ) : (
            <BoardLists lists={lists} />
          )}
        </div>
        {modal}
      </main>
    </React.Fragment>
  );
};

const mapStateToProps = (state: RootState) => ({
  lists: getBoardLists(state),
  userData: getUserData(state),
  isModalRegOpen: getStatusModalReg(state),
});

const mapDispatchToProps = (dispatch: (action: DispatchType) => void) => ({
  onChangeStatusModalReg: (statusModal: boolean) => {
    dispatch(ApplicationActionCreator.changeStatusOpenModalReg(statusModal));
  },
  onRegistration: (userData: IUserData) => {
    dispatch(ApplicationActionCreator.changeStatusOpenModalReg(false));
    dispatch(UserActionCreator.authorizeUser(AuthStatus.USER_AUTH, userData));
  },
});

export { App };
export default connect(mapStateToProps, mapDispatchToProps)(App);
