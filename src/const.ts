import { IList } from "./types/types";

const LISTS_BASE: Array<IList> = [
  {
    id: 1,
    listName: `TODO`,
    cards: [],
  },
  {
    id: 2,
    listName: `In Progress`,
    cards: [],
  },
  {
    id: 3,
    listName: `Testing`,
    cards: [],
  },
  {
    id: 4,
    listName: `Done`,
    cards: [],
  },
];

enum AuthStatus {
  USER_AUTH = `USER_AUTH`,
  USER_NO_AUTH = `USER_NO_AUTH`,
}

export { LISTS_BASE, AuthStatus };
