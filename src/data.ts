import { NAME_SPACE } from "./reducers/namespace";
import { IList, IUserData } from "./types/types";

class Data {
  isSupportLocalStorage: boolean;

  constructor() {
    this.isSupportLocalStorage = this.checkSupportLocalStorage();
  }

  checkSupportLocalStorage(): boolean {
    return typeof Storage !== "undefined" ? true : false;
  }

  getUserData(): IUserData {
    if (this.isSupportLocalStorage) {
      const NAME_PROPERTY = NAME_SPACE.USER;
      const data = JSON.parse(localStorage.getItem("data") || `{}`);

      const isNoEmptyData = Object.prototype.hasOwnProperty.call(data, NAME_PROPERTY);

      if (isNoEmptyData) {
        return data[NAME_PROPERTY].userData;
      } else {
        return { firstName: `` };
      }
    } else {
      return { firstName: `` };
    }
  }

  getListsData(): Array<IList> | [] {
    if (this.isSupportLocalStorage) {
      const NAME_PROPERTY = NAME_SPACE.DATA;
      const data = JSON.parse(localStorage.getItem("data") || `{}`);

      const isNoEmptyData = Object.prototype.hasOwnProperty.call(data, NAME_PROPERTY);

      if (isNoEmptyData) {
        return data[NAME_PROPERTY].lists;
      } else {
        return [];
      }
    } else {
      return [];
    }
  }
}

export { Data };
