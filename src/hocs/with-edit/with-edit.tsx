import React, { useState } from "react";
import {Subtract} from "utility-types";

interface InjectedProps {
  isEdit: boolean;
  onChangeStatusEdit: () => void;
}

const withEdit = (Component: any) => {
  type P = React.ComponentProps<typeof Component>;
  type T = Subtract<P, InjectedProps>;

  return (props: T) => {
    const [isEdit, updateStatusEdit] = useState(false);

    const handleChangeStatusEdit = (): void => {
      updateStatusEdit(!isEdit);
    };

    return (
      <Component
        {...props}
        isEdit={isEdit}
        onChangeStatusEdit={handleChangeStatusEdit}
      />
    );
  };
}

export { withEdit };
