import { data } from "./index";
import { IList, ICard, IComment } from "./types/types";
import { AppState } from "./reducers/application/types";
import { DataState } from "./reducers/data/types";
import { UserState } from "./reducers/user/types";

type VariantTypeState = AppState | DataState | UserState;
type AllVariantData = Array<IList> | Array<ICard> | Array<IComment>;
type OneOfData = IList | ICard | IComment;

export const extend = (a: VariantTypeState, b: VariantTypeState): VariantTypeState  => {
  return Object.assign({}, a, b);
};

export const KEYS = {
  ENTER_KEY: `Enter`,
  ESCAPE_KEY: `Escape`,
};

export const closeModals = {
  isEscPress(evt: KeyboardEvent, action: () => void): void {
    if (evt.key === KEYS.ESCAPE_KEY) {
      action();
    }
  },
  isClickOut(evt: React.MouseEvent<HTMLDivElement>, action: () => void): void {
    const target = evt.target as HTMLDivElement;
    if (target.closest(".modal__window") === null) {
      action();
    }
  },
};

const getLastId = (lists: AllVariantData): number => {
  if (lists.length === 0) {
    return 0;
  }

  const listsSort = lists
    .slice()
    .sort((a: OneOfData, b: OneOfData) => a.id - b.id);

  return listsSort[listsSort.length - 1].id;
};

const getAllCards = (lists: Array<IList>): Array<ICard> => {
  const allCards: Array<ICard> = [];

  lists.forEach((list) => {
    if (list.cards.length > 0) {
      return list.cards.forEach((card) => allCards.push(card));
    }

    return ``;
  });

  return allCards;
};

export const getNewList = (listName: string, lists: Array<IList>): IList => {
  let id = getLastId(lists);
  ++id;
  return {
    id,
    listName,
    cards: [],
  };
};

export const getNewCard = (cardName: string, lists: Array<IList>): ICard => {
  const userData = data.getUserData();
  const allCards = getAllCards(lists);
  let id = getLastId(allCards);

  return {
    id: ++id,
    cardName,
    description: ``,
    comments: [],
    author: userData.firstName,
  };
};

export const getNewComment = (
  comment: string,
  comments: Array<IComment>
): IComment => {
  const userData = data.getUserData();
  let id = getLastId(comments);

  return {
    id: ++id,
    text: comment,
    authorName: userData.firstName,
  };
};
