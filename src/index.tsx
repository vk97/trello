import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import { rootReducer } from "./reducers/reducer";
import "./index.css";
import App from "./components/App";
import { ActionCreator as DataActionCreator } from "./reducers/data/actions";
import { ActionCreator as UserActionCreator } from "./reducers/user/actions";
import { AuthStatus } from "./const";
import { ActionCreator as ApplicationActionCreator } from "./reducers/application/actions";
import { Data } from "./data";
import { LISTS_BASE } from "./const";

const data = new Data();
const store = createStore(rootReducer, composeWithDevTools());

const loadingUserData = () => {
  store.dispatch(UserActionCreator.changeStatusLoadingUserData(true));
  const userData = data.getUserData();
  store.dispatch(UserActionCreator.changeStatusLoadingUserData(false));
  store.dispatch(UserActionCreator.changeStatusLoadedUserData(true));

  const isEmptyUserData = userData.firstName.length === 0;

  if (isEmptyUserData) {
    store.dispatch(ApplicationActionCreator.changeStatusOpenModalReg(true));
  } else {
    store.dispatch(
      UserActionCreator.authorizeUser(AuthStatus.USER_AUTH, userData)
    );
  }
};

const loadingListsData = () => {
  const listsData = data.getListsData();
  let inNoEmptyListData;

  if (listsData) {
    inNoEmptyListData = !!listsData.length;
  }

  if (inNoEmptyListData) {
    store.dispatch(DataActionCreator.loadLists(listsData));
  } else {
    store.dispatch(DataActionCreator.loadLists(LISTS_BASE));
  }
};

loadingUserData();
loadingListsData();

store.subscribe(() => {
  const isSupportLocalStorage = data.checkSupportLocalStorage();
  if (isSupportLocalStorage) {
    localStorage.setItem("data", JSON.stringify(store.getState()));
  }
});

ReactDOM.render(
  <Provider store={store}><App /></Provider>,
  document.getElementById("root")
);

export { data };
