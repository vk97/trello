import { IUserData } from "../../types/types";
import { AuthStatus } from "../../const";

export const REQUIRED_AUTH = `REQUIRED_AUTH`;
export const CHANGE_STATUS_LOADING_USER_DATA = `CHANGE_STATUS_LOADING_USER_DATA`;
export const CHANGE_STATUS_LOADED_USER_DATA = `CHANGE_STATUS_LOADED_USER_DATA`;

export interface UserState {
  authStatus: AuthStatus;
  userData: IUserData;
  isLoadingUserAuth: boolean;
  isLoadedUserAuth: boolean;
}

interface IAuthorizeUser {
  type: typeof REQUIRED_AUTH;
  payload: {
    status: typeof AuthStatus.USER_AUTH;
    userData: IUserData;
  };
}

interface IChangeStatusLoadingUserData {
  type: typeof CHANGE_STATUS_LOADING_USER_DATA;
  payload: boolean;
}

interface IChangeStatusLoadedUserData {
  type: typeof CHANGE_STATUS_LOADED_USER_DATA;
  payload: boolean;
}

export type UserActionType =
  | IAuthorizeUser
  | IChangeStatusLoadingUserData
  | IChangeStatusLoadedUserData;
