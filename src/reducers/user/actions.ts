import { IUserData } from "../../types/types";
import {
  REQUIRED_AUTH,
  CHANGE_STATUS_LOADING_USER_DATA,
  CHANGE_STATUS_LOADED_USER_DATA,
  UserActionType,
} from "./types";
import { AuthStatus } from "../../const";

const ActionCreator = {
  authorizeUser: (status: AuthStatus.USER_AUTH, userData: IUserData): UserActionType => ({
    type: REQUIRED_AUTH,
    payload: {
      status,
      userData,
    },
  }),
  changeStatusLoadingUserData: (isLoading: boolean): UserActionType => ({
    type: CHANGE_STATUS_LOADING_USER_DATA,
    payload: isLoading,
  }),
  changeStatusLoadedUserData: (isLoaded: boolean): UserActionType => ({
    type: CHANGE_STATUS_LOADED_USER_DATA,
    payload: isLoaded,
  }),
};

export { ActionCreator };
