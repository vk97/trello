import {
  REQUIRED_AUTH,
  CHANGE_STATUS_LOADING_USER_DATA,
  CHANGE_STATUS_LOADED_USER_DATA,
  UserState,
  UserActionType,
} from "./types";
import { AuthStatus } from "../../const";

const initialState: UserState = {
  authStatus: AuthStatus.USER_NO_AUTH,
  userData: {
    firstName: ``,
  },
  isLoadingUserAuth: false,
  isLoadedUserAuth: false,
};

const reducer = (store = initialState, action: UserActionType): UserState => {
  switch (action.type) {
    case REQUIRED_AUTH:
      return {
        ...store,
        authStatus: action.payload.status,
        userData: action.payload.userData,
      };
    case CHANGE_STATUS_LOADING_USER_DATA:
      return {
        ...store,
        isLoadingUserAuth: action.payload,
      };
    case CHANGE_STATUS_LOADED_USER_DATA:
      return {
        ...store,
        isLoadedUserAuth: action.payload,
      };
    default:
      return store;
  }
};

export { AuthStatus, reducer };
