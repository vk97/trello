import { IUserData } from "../../types/types";
import { NAME_SPACE } from "../namespace";
import { RootState } from "../reducer";

const NAME_PROPERTY = NAME_SPACE.USER;

export const getUserData = (state: RootState): IUserData => {
  return state[NAME_PROPERTY].userData;
};
