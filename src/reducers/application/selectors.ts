import { NAME_SPACE } from "../namespace";
import { RootState } from "../reducer";

const NAME_PROPERTY = NAME_SPACE.APPLICATION;

const getStatusModalReg = (state: RootState): boolean => {
  return state[NAME_PROPERTY].isModalRegOpen;
};

const getActiveCardModal = (state: RootState): number => {
  return state[NAME_PROPERTY].activeCardModal;
};

export { getStatusModalReg, getActiveCardModal };
