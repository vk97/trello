export const CHANGE_STATUS_OPEN_MODAL_REG = `CHANGE_STATUS_OPEN_MODAL_REG`;
export const CHANGE_ACTIVE_MODAL_CARD = `CHANGE_ACTIVE_MODAL_CARD`;

export interface AppState {
  isModalRegOpen: boolean;
  activeCardModal: number;
}

interface IChangeStatusOpenModalReg {
  type: typeof CHANGE_STATUS_OPEN_MODAL_REG;
  payload: boolean;
}

interface IChangeActiveCardModal {
  type: typeof CHANGE_ACTIVE_MODAL_CARD;
  payload: number;
}

export type ApplicationActionType =
  | IChangeStatusOpenModalReg
  | IChangeActiveCardModal;
