import {
  CHANGE_STATUS_OPEN_MODAL_REG,
  CHANGE_ACTIVE_MODAL_CARD,
  AppState,
  ApplicationActionType,
} from "./types";

const initialState: AppState = {
  isModalRegOpen: false,
  activeCardModal: -1,
};

const reducer = (
  store = initialState,
  action: ApplicationActionType
): AppState => {
  switch (action.type) {
    case CHANGE_STATUS_OPEN_MODAL_REG:
      return {
        ...store,
        isModalRegOpen: action.payload,
      };
    case CHANGE_ACTIVE_MODAL_CARD:
      return {
        ...store,
        activeCardModal: action.payload,
      };
    default:
      return store;
  }
};

export { reducer };
