import {
  CHANGE_STATUS_OPEN_MODAL_REG,
  CHANGE_ACTIVE_MODAL_CARD,
  ApplicationActionType,
} from "./types";

const ActionCreator = {
  changeStatusOpenModalReg: (isOpen: boolean): ApplicationActionType => ({
    type: CHANGE_STATUS_OPEN_MODAL_REG,
    payload: isOpen,
  }),
  changeActiveCardModal: (activeCard: number): ApplicationActionType => ({
    type: CHANGE_ACTIVE_MODAL_CARD,
    payload: activeCard,
  }),
};

export { ActionCreator };
