import { IList, ICard, IComment } from "../../types/types";
import {
  GET_LISTS,
  UPDATE_LISTS,
  ADD_NEW_LIST,
  DELETE_LIST,
  ADD_NEW_CARD,
  UPDATE_CARD,
  DELETE_CARD,
  ADD_NEW_COMMENT_CARD,
  DELETE_COMMENT,
  UPDATE_COMMENTS,
  DataState,
  DataActionType,
} from "./types";

const initialState: DataState = {
  lists: [],
};

const reducer = (store = initialState, action: DataActionType): DataState => {
  switch (action.type) {
    case GET_LISTS: {
      return {
        ...store,
        lists: action.payload,
      };
    }
    case UPDATE_LISTS: {
      const indexNewList = store.lists.findIndex(
        (list: IList) => list.id === action.payload.id
      );
      return {
        ...store,
        lists: ([] as IList[]).concat(
          store.lists.slice(0, indexNewList),
          action.payload,
          store.lists.slice(indexNewList + 1)
        ),
      };
    }
    case ADD_NEW_LIST: {
      return {
        ...store,
        lists: ([] as IList[]).concat(store.lists, action.payload),
      };
    }
    case DELETE_LIST: {
      const indexDeleteList = store.lists.findIndex(
        (list: IList) => list.id === action.payload.id
      );
      return {
        lists: ([] as IList[]).concat(
          store.lists.slice(0, indexDeleteList),
          store.lists.slice(indexDeleteList + 1)
        ),
      };
    }
    case ADD_NEW_CARD: {
      return {
        lists: store.lists.map((list: IList) => {
          if (list.id === action.payload.listId) {
            return {
              ...list,
              cards: ([] as ICard[]).concat(list.cards, action.payload.card),
            };
          } else {
            return list;
          }
        }),
      };
    }
    case UPDATE_CARD: {
      return {
        ...store,
        lists: store.lists.map((list: IList) => {
          if (list.id === action.payload.listId) {
            const indexUpdateCard = list.cards.findIndex((card: ICard) => {
              return card.id === action.payload.card.id;
            });
            return {
              ...list,
              cards: ([] as ICard[]).concat(
                list.cards.slice(0, indexUpdateCard),
                action.payload.card,
                list.cards.slice(indexUpdateCard + 1)
              ),
            };
          } else {
            return list;
          }
        }),
      };
    }
    case DELETE_CARD: {
      return {
        lists: store.lists.map((list: IList) => {
          if (list.id === action.payload.listId) {
            const indexDeleteCard = list.cards.findIndex(
              (card: ICard) => card.id === action.payload.cardId
            );
            return {
              ...list,
              cards: ([] as ICard[]).concat(
                list.cards.slice(0, indexDeleteCard),
                list.cards.slice(indexDeleteCard + 1)
              ),
            };
          } else {
            return list;
          }
        }),
      };
    }
    case ADD_NEW_COMMENT_CARD: {
      return {
        lists: store.lists.map((list: IList) => {
          return {
            ...list,
            cards: list.cards.map((card) => {
              if (card.id === action.payload.cardId) {
                return {
                  ...card,
                  comments: ([] as IComment[]).concat(
                    card.comments,
                    action.payload.comment
                  ),
                };
              } else {
                return card;
              }
            }),
          };
        }),
      };
    }
    case DELETE_COMMENT: {
      return {
        lists: store.lists.map((list: IList) => {
          return {
            ...list,
            cards: list.cards.map((card) => {
              const indexDeleteComment = card.comments.findIndex((comment) => {
                return comment.id === action.payload.commentId;
              });
              if (card.id === action.payload.cardId) {
                return {
                  ...card,
                  comments: ([] as IComment[]).concat(
                    card.comments.slice(0, indexDeleteComment),
                    card.comments.slice(indexDeleteComment + 1)
                  ),
                };
              } else {
                return card;
              }
            }),
          };
        }),
      };
    }
    case UPDATE_COMMENTS: {
      return {
        lists: store.lists.map((list: IList) => {
          return {
            ...list,
            cards: list.cards.map((card) => {
              const indexUpdateComment = card.comments.findIndex((comment) => {
                return comment.id === action.payload.comment.id;
              });
              if (card.id === action.payload.cardId) {
                return {
                  ...card,
                  comments: ([] as IComment[]).concat(
                    card.comments.slice(0, indexUpdateComment),
                    action.payload.comment,
                    card.comments.slice(indexUpdateComment + 1)
                  ),
                };
              } else {
                return card;
              }
            }),
          };
        }),
      };
    }
    default:
      return store;
  }
};

export { reducer };
