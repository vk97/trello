import { IList, ICard, IComment } from "../../types/types";

export const GET_LISTS = `GET_LISTS`;
export const UPDATE_LISTS = `UPDATE_LISTS`;
export const ADD_NEW_LIST = `ADD_NEW_LIST`;
export const DELETE_LIST = `DELETE_LIST`;
export const ADD_NEW_CARD = `ADD_NEW_CARD`;
export const UPDATE_CARD = `UPDATE_CARD`;
export const DELETE_CARD = `DELETE_CARD`;
export const ADD_NEW_COMMENT_CARD = `ADD_NEW_COMMENT_CARD`;
export const DELETE_COMMENT = `DELETE_COMMENT`;
export const UPDATE_COMMENTS = `UPDATE_COMMENTS`;

export interface DataState {
  lists: Array<IList>;
}

interface ILoadLists {
  type: typeof GET_LISTS;
  payload: Array<IList>;
}

interface IUpdateLists {
  type: typeof UPDATE_LISTS;
  payload: IList;
}

interface IAddNewList {
  type: typeof ADD_NEW_LIST;
  payload: IList;
}

interface IDeleteList {
  type: typeof DELETE_LIST;
  payload: IList;
}

interface IAddNewCard {
  type: typeof ADD_NEW_CARD;
  payload: {
    card: ICard;
    listId: number;
  };
}

interface IUpdateCard {
  type: typeof UPDATE_CARD;
  payload: {
    card: ICard;
    listId: number;
  };
}

interface IDeleteCard {
  type: typeof DELETE_CARD;
  payload: {
    cardId: number;
    listId: number;
  };
}

interface IAddNewCommentCard {
  type: typeof ADD_NEW_COMMENT_CARD;
  payload: {
    cardId: number;
    comment: IComment;
  };
}

interface IDeleteCommentCard {
  type: typeof DELETE_COMMENT;
  payload: {
    cardId: number;
    commentId: number;
  };
}

interface IUpdateComments {
  type: typeof UPDATE_COMMENTS;
  payload: {
    cardId: number;
    comment: IComment;
  };
}

export type DataActionType =
  | ILoadLists
  | IUpdateLists
  | IAddNewList
  | IDeleteList
  | IAddNewCard
  | IUpdateCard
  | IDeleteCard
  | IAddNewCommentCard
  | IDeleteCommentCard
  | IUpdateComments;
