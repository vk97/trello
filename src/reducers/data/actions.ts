import { IList, ICard, IComment } from "../../types/types";
import {
  GET_LISTS,
  UPDATE_LISTS,
  ADD_NEW_LIST,
  DELETE_LIST,
  ADD_NEW_CARD,
  UPDATE_CARD,
  DELETE_CARD,
  ADD_NEW_COMMENT_CARD,
  DELETE_COMMENT,
  UPDATE_COMMENTS,
  DataActionType,
} from "./types";

const ActionCreator = {
  loadLists: (lists: Array<IList>): DataActionType => ({
    type: GET_LISTS,
    payload: lists,
  }),
  updateLists: (list: IList): DataActionType => ({
    type: UPDATE_LISTS,
    payload: list,
  }),
  addNewList: (list: IList): DataActionType => ({
    type: ADD_NEW_LIST,
    payload: list,
  }),
  deleteList: (list: IList): DataActionType => ({
    type: DELETE_LIST,
    payload: list,
  }),
  addNewCard: (card: ICard, listId: number): DataActionType => ({
    type: ADD_NEW_CARD,
    payload: {
      card,
      listId,
    },
  }),
  updateCard: (card: ICard, listId: number): DataActionType => ({
    type: UPDATE_CARD,
    payload: {
      card,
      listId,
    },
  }),
  deleteCard: (cardId: number, listId: number): DataActionType => ({
    type: DELETE_CARD,
    payload: {
      cardId,
      listId,
    },
  }),
  addNewCommentCard: (cardId: number, comment: IComment): DataActionType => ({
    type: ADD_NEW_COMMENT_CARD,
    payload: {
      cardId,
      comment,
    },
  }),
  deleteCommentCard: (cardId: number, commentId: number): DataActionType => ({
    type: DELETE_COMMENT,
    payload: {
      cardId,
      commentId,
    },
  }),
  updateComments: (cardId: number, comment: IComment): DataActionType => ({
    type: UPDATE_COMMENTS,
    payload: {
      cardId,
      comment,
    },
  }),
};

export { ActionCreator };
