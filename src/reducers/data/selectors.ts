import { IList } from "../../types/types";
import { NAME_SPACE } from "../namespace";
import { RootState } from "../reducer";

const NAME_PROPERTY = NAME_SPACE.DATA;

const getBoardLists = (state: RootState): Array<IList> => {
  return state[NAME_PROPERTY].lists;
};

export { getBoardLists };
